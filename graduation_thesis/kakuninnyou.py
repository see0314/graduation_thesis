# -*- coding: utf8 -*-
import os
import numpy as np
import cupy
import cv2
import cPickle as pickle
import sppconv3
from copymodel import *
from chainer import cuda, optimizers, Variable

gpu_flag = 0
IMAGE_SIZE = 224
CHANNEL = 3

DATA_ROOT_DIR_PATH = "/home/wada/soturon/alexnet.pkl"

if gpu_flag >= 0:
    cuda.check_cuda_available()
xp = cuda.cupy if gpu_flag >= 0 else np

# 画像のあるディレクトリ
train_img_dirs = ['anger', 'disgust', 'fear', 'happy', 'neutral', 'sad', 'surprise']

test_image = []
halftes_image = []

for i, d in enumerate(train_img_dirs):
    # ./data/以下の各ディレクトリ内のファイル名取得
    files = os.listdir('./data/test27/' + d)
    for f in files:
        # 画像読み込み
        img2 = cv2.imread('./data/test27/' + d + '/' + f, cv2.IMREAD_COLOR)#, cv2.IMREAD_GRAYSCALE)
        # 1辺がIMG_SIZEの正方形にリサイズ
        img2 = cv2.resize(img2, (IMAGE_SIZE, IMAGE_SIZE))
        # 1列にして
        img2 = img2.flatten().astype(np.float32)/255.0
        test_image.append(img2)

    files = os.listdir('./data/tesyou27/padding/' + d)
    for f in files:
        # 画像読み込み
        img = cv2.imread('./data/tesyou27/padding/' + d + '/' + f, cv2.IMREAD_COLOR)  # , cv2.IMREAD_GRAYSCALE)
        # 1辺がIMG_SIZEの正方形にリサイズ
        img = cv2.resize(img, (IMAGE_SIZE, IMAGE_SIZE))
        # 1列にして
        img = img.flatten().astype(np.float32) / 255.0
        halftes_image.append(img)

print "OK1"

# numpy配列に変換
test_image = np.asarray(test_image, dtype=np.float32)
tes_image = np.reshape(test_image, (len(test_image), CHANNEL, IMAGE_SIZE, IMAGE_SIZE))
halftes_image = np.asarray(halftes_image, dtype=np.float32)
hates_image = np.reshape(halftes_image, (len(halftes_image), CHANNEL, IMAGE_SIZE, IMAGE_SIZE))

print "OK2"

original_model = pickle.load(open(DATA_ROOT_DIR_PATH))
print "OK3"
model = sppconv3.Alex()
print "OK4"
copy_model(original_model, model)

if gpu_flag >= 0:
    cuda.get_device(gpu_flag).use()
    model.to_gpu()

ans = model(Variable(chainer.cuda.to_gpu([tes_image[0]])))
print ans.data
print ans.data.shape

