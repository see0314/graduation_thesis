# -*- coding: utf8 -*-
import time
import sys
import os
import numpy as np
import cPickle as pickle
import chainer.serializers as s
import matplotlib.pyplot as plt
import batch_alex
from copymodel import *
from chainer import cuda, optimizers, Variable
from PIL import Image

BATCH_SIZE = 63
EPOCH = 500
gpu_flag = 0
NUM_CLASSES = 7

IMAGE_SIZE = 224
CHANNEL = 3

alex_PATH = "/home/wada/soturon/alexnet.pkl"
PICKLE_DUMP_PATH = "/home/wada/soturon/result.pkl"

if gpu_flag >= 0:
    cuda.check_cuda_available()

train_img_dirs = ['anger', 'disgust', 'fear', 'happy', 'neutral', 'sad', 'surprise']

train_image = []
train_label = []
test_image = []
test_label = []

for i, d in enumerate(train_img_dirs):
    files = os.listdir('./data/kakou27/' + d)
    for f in files:
        img = Image.open('./data/kakou27/' + d + '/' + f, 'r')
        r, g, b, = img.split()
        rimg = np.asarray(np.float32(r)/255.0)
        gimg = np.asarray(np.float32(g)/255.0)
        bimg = np.asarray(np.float32(b)/255.0)
        img = np.asarray([rimg, gimg, bimg])
        train_image.append(img)
        train_label.append(i)

    files = os.listdir('./data/test27/' + d)
    for f in files:
        img2 = Image.open('./data/test27/' + d + '/' + f, 'r')
        r, g, b, = img2.split()
        rimg = np.asarray(np.float32(r)/255.0)
        gimg = np.asarray(np.float32(g)/255.0)
        bimg = np.asarray(np.float32(b)/255.0)
        img2 = np.asarray([rimg, gimg, bimg])
        test_image.append(img2)
        test_label.append(i)

print "finish reading data"

train_image = np.asarray(train_image, dtype=np.float32)
train_label = np.asarray(train_label, dtype=np.int32)
test_image = np.asarray(test_image, dtype=np.float32)
test_label = np.asarray(test_label, dtype=np.int32)

train_image = np.reshape(train_image, (len(train_image), CHANNEL, IMAGE_SIZE, IMAGE_SIZE))
test_image = np.reshape(test_image, (len(test_image), CHANNEL, IMAGE_SIZE, IMAGE_SIZE))

print "finish making train and test data"

original_model = pickle.load(open(alex_PATH))
model = batch_alex.Alex()
copy_model(original_model, model)

if gpu_flag >= 0:
    cuda.get_device(gpu_flag).use()
    model.to_gpu()

optimizer = optimizers.Adam()
optimizer.setup(model)

train_loss = []
train_acc = []
test_loss = []
test_acc = []
start_time = time.clock()

for epoch in range(1, EPOCH+1):
    print("epoch %d" % epoch)
    sys.stdout.flush()
    indices = np.random.permutation(len(train_image))
    sum_train_accuracy = 0
    sum_train_loss = 0

    for i in range(0, len(train_image), BATCH_SIZE):
        image = Variable(chainer.cuda.to_gpu(train_image[indices[i:i+BATCH_SIZE]]))
        label = Variable(chainer.cuda.to_gpu(train_label[indices[i:i+BATCH_SIZE]]))
        optimizer.zero_grads()
        loss = model(image, label, True)
        loss.backward()
        optimizer.update()

        sum_train_loss += loss.data * BATCH_SIZE
        sum_train_accuracy += model.accuracy.data * BATCH_SIZE

    print("train mean loss {a}, accuracy {b}".format(
        a=sum_train_loss/len(train_image), b=sum_train_accuracy/len(train_image)))
    train_loss.append(sum_train_loss/len(train_image))
    train_acc.append(sum_train_accuracy/len(train_image))
    sys.stdout.flush()


    """
    test
    """
    sum_test_accuracy = 0
    sum_test_loss = 0
    for i in range(0, len(test_image), BATCH_SIZE):
        image2 = chainer.Variable(chainer.cuda.to_gpu(test_image[i:i + BATCH_SIZE]))
        label2 = chainer.Variable(chainer.cuda.to_gpu(test_label[i:i + BATCH_SIZE]))
        loss2 = model(image2, label2, False)

        sum_test_loss += loss2.data * BATCH_SIZE
        sum_test_accuracy += model.accuracy.data * BATCH_SIZE

    print("test mean loss {a}, accuracy {b}".format(
        a=sum_test_loss / len(test_image), b=sum_test_accuracy / len(test_image)))
    test_loss.append(sum_test_loss / len(test_image))
    test_acc.append(sum_test_accuracy / len(test_image))
    sys.stdout.flush()

end_time = time.clock()
print "total time = " + str(end_time - start_time)

pickle.dump(model, open(PICKLE_DUMP_PATH, "wb"))

epoch = len(train_loss)
plt.plot(range(epoch), train_loss, marker='.', label='loss')
plt.legend(loc='best', fontsize=10)
plt.grid()
plt.xlabel('epoch')
plt.ylabel('loss')
filename = "yamada_loss.png"
plt.savefig(filename)
plt.show()

plt.plot(range(epoch), train_acc, marker='.', label='train_acc')
plt.plot(range(len(test_acc)), test_acc, marker='.', label='test_acc')
plt.legend(loc='best', fontsize=10)
plt.grid()
plt.xlabel('epoch')
plt.ylabel('acc')
filename = "yamada_acc.png"
plt.savefig(filename)
plt.show()
