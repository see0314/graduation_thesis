# -*- coding: utf8 -*-
import csv
import numpy as np

NUM_CLASSES = 2

f = open('fer2013sort2ti.csv', 'r')
image = []
label = []
onehot_label = []
tr_img = []
tr_label = []
te_img = []
te_label = []

for line in f:
    line = line.rstrip()
    l = line.split(",")
    l[1] = l[1].split()

    image.append(l[1])
    label.append(l[0])

image.pop(0)
label.pop(0)

print "OK1"

for i in range(0, len(image)):
    for j in range(0, len(image[i])):
        image[i][j] = float(image[i][j]) / 255.0

flabel = map(int, label)

for k in range(0, len(flabel)):
        # one_hot_vectorを作りラベルとして追加
        tmp = np.zeros(NUM_CLASSES)
        tmp[flabel[k]] = 1
        onehot_label.append(tmp)

print "OK2"

for i in range(0, 28707):
    tr_img.append(image[i])
    tr_label.append(onehot_label[i])

for i in range(28707, 35885):
    te_img.append(image[i])
    te_label.append(onehot_label[i])

print "OK3"

# numpy配列に変換
train_image = np.asarray(tr_img)
train_label = np.asarray(tr_label)
test_image = np.asarray(te_img)
test_label = np.asarray(te_label)

print "OK4"

np.savetxt('2sort_image.csv', train_image, delimiter=',')
np.savetxt('2sort_label.csv', train_label, delimiter=',')
np.savetxt('2sort_test_image.csv', test_image, delimiter=',')
np.savetxt('2sort_test_label.csv', test_label, delimiter=',')

print "OK5"

rimage = np.loadtxt('2sort_image.csv', delimiter=',')
rlabel = np.loadtxt('2sort_label.csv', delimiter=',')
eimage = np.loadtxt('2sort_test_image.csv', delimiter=',')
elabel = np.loadtxt('2sort_test_label.csv', delimiter=',')

print(rimage.shape)
print (rlabel.shape)
print(eimage.shape)
print (elabel.shape)

f.close()