# -*- coding: utf-8 -*-
import cv2
import os

def left():
    train_img_dirs = ['anger', 'disgust', 'fear', 'happy', 'neutral', 'sad', 'surprise']

    for i, d in enumerate(train_img_dirs):
        # ./data/以下の各ディレクトリ内のファイル名取得
        files = os.listdir('./data/face27/' + d)
        for f in files:
            img = cv2.imread('./data/face27/' + d + '/' + f)
            height, width = img.shape[:2]

            clp = img[0:height, width/2:width]
            cv2.imwrite('./data/trayou27/left/' + d + '/' + f, clp)

def right():
    train_img_dirs = ['anger', 'disgust', 'fear', 'happy', 'neutral', 'sad', 'surprise']

    for i, d in enumerate(train_img_dirs):
        # ./data/以下の各ディレクトリ内のファイル名取得
        files = os.listdir('./data/face27/' + d)
        for f in files:
            img = cv2.imread('./data/face27/' + d + '/' + f)
            height, width = img.shape[:2]

            img[0:height, width/2:width] = [255, 255, 255]
            cv2.imwrite('./data/right_white/' + d + '/' + f, img)

def flip():
    train_img_dirs = ['anger', 'disgust', 'fear', 'happy', 'neutral', 'sad', 'surprise']

    for i, d in enumerate(train_img_dirs):
        # ./data/以下の各ディレクトリ内のファイル名取得
        files = os.listdir('./data/trayou27/left/' + d)
        for f in files:
            img = cv2.imread('./data/trayou27/left/' + d + '/' + f)

            yAxis = cv2.flip(img, 1)

            cv2.imwrite('./data/trayou27/flip/' + d + '/' + f, yAxis)

def bound():
    train_img_dirs = ['anger', 'disgust', 'fear', 'happy', 'neutral', 'sad', 'surprise']

    for i, d in enumerate(train_img_dirs):
        # ./data/以下の各ディレクトリ内のファイル名取得
        files = os.listdir('./data/trayou27/left/' + d)
        for f in files:
            imgleft = cv2.imread('./data/trayou27/left/' + d + '/' + f)
            imgflip = cv2.imread('./data/trayou27/flip/' + d + '/' + f)

            imgbound = cv2.hconcat([imgflip, imgleft])  # 横方向の連結
            cv2.imwrite('./data/trayou27/bound/' + d + '/' + f, imgbound)

def padding():
    train_img_dirs = ['anger', 'disgust', 'fear', 'happy', 'neutral', 'sad', 'surprise']

    for i, d in enumerate(train_img_dirs):
        # ./data/以下の各ディレクトリ内のファイル名取得
        files = os.listdir('./data/kakou27/' + d)
        for f in files:
            img = cv2.imread('./data/kakou27/' + d + '/' + f)
            height, width = img.shape[:2]

            img[0:height, 0:width/2] = [0, 0, 0]
            cv2.imwrite('./data/padding/' + d + '/' + f, img)

def resize():
    train_img_dirs = ['anger', 'disgust', 'fear', 'happy', 'neutral', 'sad', 'surprise']

    for i, d in enumerate(train_img_dirs):
        # ./data/以下の各ディレクトリ内のファイル名取得
        files = os.listdir('./data/trayou27/left/' + d)
        for f in files:
            img = cv2.imread('./data/trayou27/left/' + d + '/' + f)
            height, width = img.shape[:2]

            size = cv2.resize(img, (height, width*2))

            cv2.imwrite('./data/trayou27/resize/' + d + '/' + f, size)

if __name__ == '__main__':
    right()
    left()
    flip()
    bound()
    padding()
    resize()
