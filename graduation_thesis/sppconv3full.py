# -*- coding: utf8 -*-
import chainer
import chainer.functions as F
import chainer.links as L

class Alex(chainer.Chain):

    """Single-GPU AlexNet without partition toward the channel axis."""

    insize = 227

    def __init__(self):
        super(Alex, self).__init__(
            conv1=L.Convolution2D(3,  96, 11, stride=4),
            bn1=L.BatchNormalization(96),
            conv2=L.Convolution2D(96, 256,  5, pad=2),
            bn2=L.BatchNormalization(256),
            conv3=L.Convolution2D(256, 384,  3, pad=1),
            bn3=L.BatchNormalization(384),
            conv4=L.Convolution2D(384, 384,  3, pad=1),
            bn4=L.BatchNormalization(384),
            conv5=L.Convolution2D(384, 256,  3, pad=1),
            bn5=L.BatchNormalization(256),
            fc5_1=L.Linear(256, 256),
            fc5_2=L.Linear(1024, 1024),
            fc5_4=L.Linear(4096, 4096),
            fc6=L.Linear(5376, 4096),
            fc7=L.Linear(4096, 4096),
            emotion_fc8=L.Linear(4096, 7),
        )
        self.train = True

    def clear(self):
        self.loss = None
        self.accuracy = None

    def __call__(self, x, t):
        self.clear()
        h = F.max_pooling_2d(self.bn1(F.relu(self.conv1(x))), 3, stride=2)
        h = F.max_pooling_2d(self.bn2(F.relu(self.conv2(h))), 3, stride=2)
        h = F.max_pooling_2d(self.bn3(F.relu(self.conv3(h))), 13, stride=13)
        h = self.bn4(F.relu(self.conv4(h)))
        h = self.bn5(F.relu(self.conv5(h)))
        h = F.dropout(F.relu(self.fc5_1(h)), train=self.train, ratio=0.5)

        g = F.max_pooling_2d(self.bn1(F.relu(self.conv1(x))), 3, stride=2)
        g = F.max_pooling_2d(self.bn2(F.relu(self.conv2(g))), 3, stride=2)
        g = F.max_pooling_2d(self.bn3(F.relu(self.conv3(g))), 7, stride=6)
        g = self.bn4(F.relu(self.conv4(g)))
        g = self.bn5(F.relu(self.conv5(g)))
        g = F.dropout(F.relu(self.fc5_2(g)), train=self.train, ratio=0.5)

        f = F.max_pooling_2d(self.bn1(F.relu(self.conv1(x))), 3, stride=2)
        f = F.max_pooling_2d(self.bn2(F.relu(self.conv2(f))), 3, stride=2)
        f = F.max_pooling_2d(self.bn3(F.relu(self.conv3(f))), 4, stride=3)
        f = self.bn4(F.relu(self.conv4(f)))
        f = self.bn5(F.relu(self.conv5(f)))
        f = F.dropout(F.relu(self.fc5_4(f)), train=self.train, ratio=0.5)

        e = F.concat((h, g, f), axis=1)

        e = F.dropout(F.relu(self.fc6(e)), train=self.train, ratio=0.5)
        e = F.dropout(F.relu(self.fc7(e)), train=self.train, ratio=0.5)
        e = self.emotion_fc8(e)

        self.loss = F.softmax_cross_entropy(e, t)
        self.accuracy = F.accuracy(e, t)
        return self.loss
