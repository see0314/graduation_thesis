# -*- coding: utf8 -*-
import chainer.functions.caffe as c
import chainer.serializers as s
import vgg16
import cPickle as pickle
import alex
from chainer.links.caffe import CaffeFunction

# func = c.CaffeFunction('bvlc_alexnet.caffemodel') # caffemodel 読み込み。遅い！
# s.save_hdf5('alexnet.h5', func) # hdf5 ファイルに保存。簡単！
# pickle.dump(func, open('alex.pkl', 'wb'))

# func = CaffeFunction('VGG_ILSVRC_16_layers.caffemodel')
# pickle.dump(func, open('vgg16.pkl', 'wb'))
#
# func = CaffeFunction('VGG_ILSVRC_19_layers.caffemodel')
# pickle.dump(func, open('vgg19.pkl', 'wb'))
model = vgg16.VGGNet()
s.load_hdf5('VGG.model', model)
pickle.dump(model, open('VGG.pkl', 'wb'))
