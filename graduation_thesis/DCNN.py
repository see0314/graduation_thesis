# -*- coding: utf8 -*-
import chainer
import chainer.functions as F
import chainer.links as L
import spp1234

class Alex(chainer.Chain):

    """Single-GPU AlexNet without partition toward the channel axis."""

    insize = 227

    def __init__(self):
        super(Alex, self).__init__(
            conv1=L.Convolution2D(3,  96, 11, stride=4),
            bn1=L.BatchNormalization(96),
            conv2=L.Convolution2D(96, 256,  5, pad=2),
            bn2=L.BatchNormalization(256),
            conv3=L.Convolution2D(256, 384,  3, pad=1),
            bn3=L.BatchNormalization(384),
            conv4=L.Convolution2D(384, 384,  3, pad=1),
            bn4=L.BatchNormalization(384),
            conv5=L.Convolution2D(384, 256,  3, pad=1),
            bn5=L.BatchNormalization(256),
            fc6=L.Linear(9216, 4096),
            # fc6=L.Linear(7680, 4096),
            fc7=L.Linear(4096, 4096),
            emotion_fc8=L.Linear(8192, 7),
            # emotion_fc8=L.Linear(4096, 7),
        )
        self.train = True

    def clear(self):
        self.loss = None
        self.accuracy = None

    def __call__(self, x, y, t, s, train):
        self.clear()
        self.train = train
        h = F.max_pooling_2d(F.relu(self.bn1(self.conv1(x))), 3, stride=2)
        h = F.max_pooling_2d(F.relu(self.bn2(self.conv2(h))), 3, stride=2)
        h = F.relu(self.bn3(self.conv3(h)))
        h = F.relu(self.bn4(self.conv4(h)))
        # h = spp1234.spatial_pyramid_pooling_2d(F.relu(self.bn5(self.conv5(h))), 4, F.MaxPooling2D)
        h = F.max_pooling_2d(F.relu(self.bn5(self.conv5(h))), 3, stride=2)
        h = F.dropout(F.relu(self.fc6(h)), train=self.train, ratio=0.5)
        h = F.dropout(F.relu(self.fc7(h)), train=self.train, ratio=0.5)

        g = F.max_pooling_2d(F.relu(self.bn1(self.conv1(y))), 3, stride=2)
        g = F.max_pooling_2d(F.relu(self.bn2(self.conv2(g))), 3, stride=2)
        g = F.relu(self.bn3(self.conv3(g)))
        g = F.relu(self.bn4(self.conv4(g)))
        # g = spp1234.spatial_pyramid_pooling_2d(F.relu(self.bn5(self.conv5(g))), 4, F.MaxPooling2D)
        g = F.max_pooling_2d(F.relu(self.bn5(self.conv5(g))), 3, stride=2)
        g = F.dropout(F.relu(self.fc6(g)), train=self.train, ratio=0.5)
        g = F.dropout(F.relu(self.fc7(g)), train=self.train, ratio=0.5)

        f = F.concat((h, g), axis=1)

        f = self.emotion_fc8(f)

        self.loss = F.softmax_cross_entropy(f, t)
        self.accuracy = F.accuracy(f, t)
        return self.loss

