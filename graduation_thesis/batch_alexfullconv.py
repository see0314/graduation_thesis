# -*- coding: utf8 -*-
import chainer
import chainer.functions as F
import chainer.links as L


class Alex(chainer.Chain):

    """Single-GPU AlexNet without partition toward the channel axis."""

    insize = 227

    def __init__(self):
        super(Alex, self).__init__(
            conv1=L.Convolution2D(3,  96, 11, stride=4),
            bn1=L.BatchNormalization(96),
            conv2=L.Convolution2D(96, 256,  5, pad=2),
            bn2=L.BatchNormalization(256),
            conv3=L.Convolution2D(256, 384,  3, pad=1),
            bn3=L.BatchNormalization(384),
            conv4=L.Convolution2D(384, 384,  3, pad=1),
            bn4=L.BatchNormalization(384),
            conv5=L.Convolution2D(384, 256,  3, pad=1),
            bn5=L.BatchNormalization(256),
            fc6=L.Linear(9216, 4096),
            bn6=L.BatchNormalization(4096),
            fc7=L.Linear(4096, 4096),
            bn7=L.BatchNormalization(4096),
            emotion_fc8=L.Linear(4096, 7),
            bn8=L.BatchNormalization(7),
        )
        self.train = True

    def clear(self):
        self.loss = None
        self.accuracy = None

    def __call__(self, x, t):
        self.clear()
        h = F.max_pooling_2d(F.relu(self.bn1(self.conv1(x))), 3, stride=2)
        h = F.max_pooling_2d(F.relu(self.bn2(self.conv2(h))), 3, stride=2)
        h = F.relu(self.bn3(self.conv3(h)))
        h = F.relu(self.bn4(self.conv4(h)))
        h = F.max_pooling_2d(F.relu(self.bn5(self.conv5(h))), 3, stride=2)
        h = F.dropout(F.relu(self.bn6(self.fc6(h))), train=self.train, ratio=0.5)
        h = F.dropout(F.relu(self.bn7(self.fc7(h))), train=self.train, ratio=0.5)
        h = self.bn8(self.emotion_fc8(h))
        # return h
        self.loss = F.softmax_cross_entropy(h, t)
        self.accuracy = F.accuracy(h, t)
        return self.loss

