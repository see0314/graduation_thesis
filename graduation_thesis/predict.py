# -*- coding: utf8 -*-
import sys
import os
import numpy as np
import cupy
import cv2
import cPickle as pickle
import emotion_alex
import batch_alex
import batch_alexfullconv
import DCNN
from copymodel import *
from chainer import cuda, optimizers, Variable
from chainer import serializers

gpu_flag = 0
IMAGE_SIZE = 224
CHANNEL = 3

# DATA_ROOT_DIR_PATH = "/home/wada/soturon/result/alex_result.pkl"
# DATA_ROOT_DIR_PATH = "/home/wada/soturon/result/batchalexfullresult.pkl"
# DATA_ROOT_DIR_PATH = "/home/wada/soturon/result/alexbatch_result.pkl"
DATA_ROOT_DIR_PATH = "/home/wada/soturon/result/dcnnspp_result.pkl"

if gpu_flag >= 0:
    cuda.check_cuda_available()
xp = cuda.cupy if gpu_flag >= 0 else np

# 画像のあるディレクトリ
train_img_dirs = ['anger', 'disgust', 'fear', 'happy', 'neutral', 'sad', 'surprise']

test_image = []
test_label = []
halftes_image = []
halftes_label = []

for i, d in enumerate(train_img_dirs):
    # ./data/以下の各ディレクトリ内のファイル名取得
    files = os.listdir('./data/test27/' + d)
    for f in files:
        # 画像読み込み
        img2 = cv2.imread('./data/test27/' + d + '/' + f, cv2.IMREAD_COLOR)#, cv2.IMREAD_GRAYSCALE)
        # 1辺がIMG_SIZEの正方形にリサイズ
        img2 = cv2.resize(img2, (IMAGE_SIZE, IMAGE_SIZE))
        # 1列にして
        img2 = img2.flatten().astype(np.float32)/255.0
        test_image.append(img2)

        test_label.append(i)

    files = os.listdir('./data/tesyou27/padding/' + d)
    for f in files:
        # 画像読み込み
        img = cv2.imread('./data/tesyou27/padding/' + d + '/' + f, cv2.IMREAD_COLOR)  # , cv2.IMREAD_GRAYSCALE)
        # 1辺がIMG_SIZEの正方形にリサイズ
        img = cv2.resize(img, (IMAGE_SIZE, IMAGE_SIZE))
        # 1列にして
        img = img.flatten().astype(np.float32) / 255.0
        halftes_image.append(img)

        halftes_label.append(i)

print "OK1"

# numpy配列に変換
test_image = np.asarray(test_image, dtype=np.float32)
test_label = np.asarray(test_label)
tes_image = np.reshape(test_image, (len(test_image), CHANNEL, IMAGE_SIZE, IMAGE_SIZE))
halftes_image = np.asarray(halftes_image, dtype=np.float32)
halftes_label = np.asarray(halftes_label, dtype=np.int32)
hates_image = np.reshape(halftes_image, (len(halftes_image), CHANNEL, IMAGE_SIZE, IMAGE_SIZE))

print "OK2"

# load an original Caffe model
original_model = pickle.load(open(DATA_ROOT_DIR_PATH))
print "OK3"
# load a new model to be fine-tuned
model = DCNN.Alex()
print "OK4"
# copy W/b from the original model to the new one
copy_model(original_model, model)

# optimizer = optimizers.Adam()
# optimizer.setup(model)
#
# serializers.load_hdf5('result.model', model)
# serializers.load_hdf5('result.state', optimizer)

if gpu_flag >= 0:
    cuda.get_device(gpu_flag).use()
    model.to_gpu()

random_num = np.random.permutation(len(test_image))
acc = 0

for i in xrange(100):
    ans = model(Variable(chainer.cuda.to_gpu([tes_image[random_num[i]]])), Variable(chainer.cuda.to_gpu([hates_image[random_num[i]]])))
    print ans.data
    print test_label[random_num[i]]
    print cupy.argmax(ans.data)
    if int(test_label[random_num[i]]) == int(cupy.argmax(ans.data)):
        acc += 1
print "total accuracy = " + str(acc)
